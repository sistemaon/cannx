const express = require('express');
const router = express.Router();

const potcore = require('potcore');
const RpcClient = potcore.RpcClient;

 const config = {
   protocol: 'http',
   user: 'potcoinrpc',
   pass: 'potcoin321',
   host: '127.0.0.1',
   port: '9420',
 };

const rpc = new RpcClient(config);

const create = ( req, res ) => {
  // console.log('rpc ::; ', rpc);
  // console.log('getInfo ::; ', rpc.getBlock());
  rpc.getInfo( (err, ret) => {
    if (err) {
      console.log(err);
      return;
    }
    console.log(ret);
    res.json({add: ret})
  });
}

const getBalance = (req, res) => {
  const account = req.body.account || 'bleutrade'
  // const account = req.body.account
  const minConf = 1

  rpc.getBalance(account, minConf, (err, balance) => {
    if (err) console.error(err)
    console.log('Balance ::; ', balance)
    res.json({'Account: ': account, 'Balance': balance})
  })

}

const getNewAddress = (req, res) => {
  const account = req.body.account || 'teste01'
  rpc.getNewAddress(account, (err, data) => {
    if (err) console.error(err)
    console.log('Address ::; ', data)
    res.json({'Address': data, 'Account': account})
    // {
    // "Address": "muRv7Csx4Knv8MjwnqrJmeJRJXuiy1iYEi",
    // "Account": ""
    // }
  })
}
// {
//     "Address": {
//         "result": "PEH1EKHjn8WKf2MySzv9RR57eCdNgbrHao",
//         "error": null,
//         "id": null
//     },
//     "Account": "teste01"
// }



// router endpoint to create game
router.get('/blck', create);
router.get('/blc', getBalance);
router.get('/bla', getNewAddress);

// router endpoint to finds all game
// router.get('/game', Game.findAll);

module.exports = router;
